from django.shortcuts import render, redirect
from .models import Status
from .forms import *


def create_status(request):
    if request.method == "POST":
        form = StatusForms(request.POST or None)
        if form.is_valid():
            status = Status()
            status.title = form.cleaned_data['title']
            status.save()
            print(status)
        return redirect('/contact')
    else:
        form = StatusForms()
        status = Status.objects.all()
        response = {'status': status, 'title': 'List', 'form': form}
        return render(request, "contact.html", response)


def home(request):
    return render(request, "home.html")


def about_func(request):
    response = {'title': 'Me'}
    return render(request, "about.html", response)
