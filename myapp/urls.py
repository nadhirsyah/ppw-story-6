from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='home'),
    path('contact/', views.create_status, name="contact"),
    path('about/', views.about_func, name="about"),
]
