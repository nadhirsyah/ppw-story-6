from django import forms
from .models import Status
class StatusForms(forms.Form):
    title = forms.CharField(label = "Status", max_length = 30, widget = forms.TextInput(
        attrs = {'placeholder':'Activity', 'required':True,}
    ))
    
